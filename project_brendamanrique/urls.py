"""project_brendamanrique URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import patterns,include, url
from django.contrib import admin
from webapp import views
from django.conf import settings
from django.conf.urls.static import static
admin.autodiscover()

urlpatterns = [
    url(r'^$', views.index, name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^about/$', 'webapp.views.about', name='about'),
    url(r'^bookme/$', 'webapp.views.bookme', name='bookme'), 
    url(r'^portfolio/$', 'webapp.views.portfolio', name='portfolio'),
    url(r'^contact/$', 'webapp.views.contact', name='contact'), 
    url(r'^portfolio_aqu/$', 'webapp.views.portfolio_aqu', name='portfolio_aqu'), 
    url(r'^portfolio_ski/$', 'webapp.views.portfolio_ski', name='portfolio_ski'),
    url(r'^portfolio_fra/$', 'webapp.views.portfolio_fra', name='portfolio_fra'),
]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
